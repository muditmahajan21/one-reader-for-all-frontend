import axios from "axios"

const getAllStackexchange = async () => {
    const request = await axios.get('https://one-reader-for-all-backend.cyclic.app/stackexchange')
    return request.data
}

export default getAllStackexchange
