import axios from "axios"

const getAllDevrant = async () => {
    const request = await axios.get('https://one-reader-for-all-backend.cyclic.app/devrant')
    return request.data 
}

export default getAllDevrant
